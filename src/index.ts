// In order for the workers runtime to find the class that implements
// our Durable Object namespace, we must export it from the root module.
import * as Y from "yjs";
import { Client, query as q } from "faunadb";

export default {
  async fetch(request: Request, env: any) {
    try {
      let url = new URL(request.url);
      let path = url.pathname.slice(1).split("/");
      switch (path[0]) {
        case "entity":
          return handleRequest(path, request, env);
        default:
          return new Response("404", { status: 404 });
      }
    } catch (e) {
      return new Response(e.message);
    }
  }
};

async function handleRequest(path: string[], request: Request, env: any) {
  let entityId = path[1];
  let id = env.ENTITY.idFromName(entityId);
  let entityObject = env.ENTITY.get(id);
  return entityObject.fetch("/" + entityId, request);
}

export class Entity {
  initializePromise: Promise<any> | undefined;
  sockets: Array<{ socket: WebSocket; id: number }>;
  nextID: number;
  doc: Y.Doc | undefined;
  state: DurableObjectState;
  initialError: string | undefined;
  env: {
    FAUNA_SECRET: string;
  };

  constructor(state: DurableObjectState, env: { FAUNA_SECRET: string }) {
    this.state = state;
    this.env = env;
    this.initializePromise = undefined;
    this.sockets = [];
    this.nextID = 0;
  }

  async initialize(entity: string) {
    this.doc = new Y.Doc();
    // Look up this entity + it's children and also set up a listener.
    //
    // Q: Does every child have it's own Doc or is there 1 per entity?
    // Seperating out these histories seems smarter, but if they are named types
    // it shouldn't really conflict. And we would just deal w/ seperating it out
    // on the clientside
    // It does mean we need to observe for changes more specifically when we do writes.
    //
    let fauna = new Client({
      secret: this.env.FAUNA_SECRET,
      fetch: customFetch
    });
    try {
      let persistedState: string = await fauna.query(
        q.Select(
          ["data", "value"],
          q.Get(
            q.Range(
              q.Match(q.Index("eav"), [
                q.Ref(q.Collection("Entities"), entity)
              ]),
              "doc/crdt",
              "doc/crdt"
            )
          )
        )
      );
      if (persistedState)
        Y.applyUpdate(
          this.doc,
          Uint8Array.from(atob(persistedState), c => c.charCodeAt(0))
        );
      this.doc.on("update", async () => {
        if (!this.doc) return;
        await fauna.query(
          q.Update(
            q.Select(
              "ref",
              q.Get(
                q.Range(
                  q.Match(q.Index("eav"), [
                    q.Ref(q.Collection("Entities"), entity)
                  ]),
                  "doc/crdt",
                  "doc/crdt"
                )
              )
            ),
            {
              data: {
                value: btoa(
                  String.fromCharCode(...Y.encodeStateAsUpdate(this.doc))
                )
              }
            }
          )
        );
      });
    } catch (e) {
      this.initialError = e;
    }
  }

  // Handle HTTP requests from clients.
  async fetch(request: Request) {
    // Make sure we're fully initialized from storage.
    let url = new URL(request.url);
    let path = url.pathname.slice(1).split("/");
    if (!this.initializePromise) {
      this.initializePromise = this.initialize(path[0]).catch(err => {
        // If anything throws during initialization then we need to be
        // sure sure that a future request will retry initialize().
        // Note that the concurrency involved in resetting this shared
        // promise on an error can be tricky to get right -- we don't
        // recommend customizing it.
        this.initializePromise = undefined;
        throw err;
      });
    }
    await this.initializePromise;
    const upgradeHeader = request.headers.get("Upgrade");

    if (!upgradeHeader || upgradeHeader !== "websocket") {
      return new Response("Expected Upgrade: websocket", { status: 426 });
    }

    let websocketPair = new WebSocketPair();
    let [client, server] = Object.values(websocketPair) as [
      WebSocket,
      WebSocket
    ];

    //@ts-ignore
    server.accept();
    server.binaryType = "arraybuffer";
    let id = this.nextID;
    server.addEventListener("message", event => {
      try {
        if (!this.doc) return;
        if (event.data instanceof ArrayBuffer) {
          let change = new Uint8Array(event.data);
          Y.applyUpdate(this.doc, change);
        }
        this.sockets.forEach(socket => {
          if (socket.id === id) return;
          socket.socket.send(event.data);
        });
      } catch (e) {
        server.send(e.message);
      }
    });
    server.addEventListener("close", () => {
      this.sockets = this.sockets.filter(s => s.id !== id);
    });

    this.sockets.push({
      id,
      socket: server
    });
    if (this.doc) server.send(Y.encodeStateAsUpdate(this.doc));
    this.nextID = this.nextID + 1;

    return new Response(null, {
      status: 101,
      //@ts-ignore
      webSocket: client
    });
  }
}

function customFetch(url: RequestInfo, params?: any): Promise<Response> {
  const signal = params.signal;
  delete params.signal;

  const abortPromise = new Promise(resolve => {
    if (signal) {
      signal.onabort = resolve;
    }
  });

  return Promise.race([abortPromise, fetch(url, params)]) as Promise<Response>;
}
